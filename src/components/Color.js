import React from 'react';

const Color = (props) => {
    console.log(props.activeColor)
    return (
        <div className="card">
            <h5 className="card-header">Color Setting</h5>
            <div className="card-body">
                {
                    props.colors.map((item, key) => (
                        <span
                            key={key}
                            style={{ backgroundColor: `${item}`}} 
                            className={ item === props.activeColor ? 'isActive ml-3 mr-3 pl-5 pt-2 pb-2' : 'ml-3 mr-3 pl-5 pt-2 pb-2' }
                            onClick={ () => props.changeColor(item) }
                        >                            
                        </span>
                    ))
                }
            </div>            
            <button type="button" class="btn btn-warning mt-2 mb-2 ml-5 mr-5 text-white" onClick={ props.resetAll }>Reset All</button>            
        </div>
    );
};

export default Color;