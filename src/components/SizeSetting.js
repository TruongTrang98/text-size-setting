import React from 'react';

const SizeSetting = (props) => {
    return (
        <div className="card">
            <h5 className="card-header">Text Sizing</h5>
            <div className="card-body">
                <p>Size: {props.size}px</p>
                <button type="button" class="btn btn-success" onClick={props.changeSizeGiam}>Giảm</button> &nbsp; &nbsp;
                <button type="button" class="btn btn-success" onClick={props.changeSizeTang} >Tăng</button>
            </div>
        </div>
    );
};

export default SizeSetting;