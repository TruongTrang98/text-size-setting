import React from 'react';

const Result = (props) => {
    console.log(props);
    return (
        <div style={{ fontSize:`${props.size}px`, color:`${props.color}`, textAlign: "center", border: `3px solid ${props.color}`, width: '100%' }} className='pt-2 pb-2'>
            This is a text with size and color changed
      </div>
    );
};

export default Result;