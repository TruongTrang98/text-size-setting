import React, { useState } from 'react';
import './App.css';
import Color from './components/Color';
import SizeSetting from './components/SizeSetting';
import Result from './components/Result';

function App() {
  const colors = ['red', 'green', 'yellow', 'blue'];
  const [color, changeColor] = useState('red')
  const [size, changeSize] = useState(12)
  return (
    <div className='App'>
      <div className="container">
        <hr/>
        <h1>TEXT SIZE SETTING</h1>
        <hr />
        <div className="row">
          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <Color colors={colors} activeColor={color} changeColor={ (newColor) => changeColor(newColor) } resetAll={ () => {
              changeSize(12)
              changeColor('red')
            } }/>
          </div>
          <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <SizeSetting changeSizeGiam={ () => changeSize(size - 2) } changeSizeTang={ () => changeSize(size + 2) } size={size}/>
          </div>
        </div>
        
        <div className="row mt-5 text-center">
          <Result color={color} size={size} />
        </div>
        
      </div>
    </div>
  );
}

export default App;
